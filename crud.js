let http = require("http");
// mock database
let directory= [
		{
			"name": "Brandon",
			"email": "brandon@mail.com"
		},
		{
			"name": "Jobert",
			"email": "jobert@mail.com"
		}
	]
http.createServer(function(request, response){

	if(request.url== "/users" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'application/json'})

		// we convert the users array into JSON since the server response in JSON format aswell
		response.write(JSON.stringify(directory));
		response.end()
	}

	if(request.url == "/users" && request.method == "POST"){
		// 1. initialize requestBody variable which will later contain the data/body from postman
		let requestBody= '';

		//2. upon receiving the data re-assign the value of requestBody to the contents of the body from postman
		request.on('data', function(data){
			requestBody += data;
		})

		// 3. before the request ends, convert the request variable from string into Javascript object
		// in order to be able to access its properties and assign them into the newUser variable
		request.on('end', function(){
			console.log(typeof requestBody)

			// converts the string requestBody to JSON
			requestBody= JSON.parse(requestBody)

			
			let newUser={
				"name": requestBody.name,
				"email": requestBody.email
			}

			// after setting the values from the requestBody to the newUser variable,
			// push the newUser variable into the mock database
			directory.push(newUser)
			console.log(directory)

			response.writeHead(200, {'Content-Type': 'application/json'})
			response.write(JSON.stringify(newUser))
			response.end();
		})
	}
}).listen(4004);

console.log('Server running at localhost: 4004')