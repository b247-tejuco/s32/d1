// use the "require" directive to load Node.js modules
let http= require("http");

http.createServer(function(request, response){
		// the HTTP Method of the incoming request can be accessed 
		// via the "method" property of the "request" parameter
		// the method "get" means that we will be retrieving or reading information
	if(request.url == "/items" && request.method == "GET"){

		// request the "/items" path and "GETS" information
		response.writeHead(200, {'Content-Type': 'text/plain'})
		// ends the response process
		response.end('Data retrieved from database!')
	}

	// the method "POST" means that we will be adding or creating information
	// In this example, we will just be sending a text response for now
	if(request.url == "/items" && request.method == "POST"){

		// request the "/items" path and "SENDS" information
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Data to be sent to the database!!')
	}
}).listen(4000)
console.log('Server running at localhost: 4000')

// HTTP Methods
// GET - RETRIEVES RESOURCES
// POST- SENDS DATA FOR CREATING RESOURCES
// PUT- SENDS DATA FOR UPDATING RESOURCES
// DELETE- DELETE A SPECIFIED RESOURCE